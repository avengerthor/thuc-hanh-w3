import 'package:flutter/material.dart';
import '../image/image.dart';
import '../image_stream/image_stream.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _HomeState();
}

class _HomeState extends State<App> {
  MyImageStream imageStream = MyImageStream();

  List<ImageModel> listImages = [];

  startStream() async {
    imageStream.getImage().listen((image) {
      setState(() {
        listImages.add(image);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    startStream();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Row(
            children: const [
              Text("Images Streaming",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1.0,
                  crossAxisSpacing: 10.0,
                  mainAxisSpacing: 10.0),
              itemCount: listImages.length,
              itemBuilder: (context, index) {
                return ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(listImages[index].url));
              },
            ),
          ),
        ],
      ),
    );
  }
}
