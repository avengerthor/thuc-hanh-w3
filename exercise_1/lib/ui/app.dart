import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../stream/color_stream.dart';
import '../stream/message_stream.dart';


class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  MessageStream messageStream = MessageStream();
  List<String> messages = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Stream Message using ListView',

        home: Scaffold(
          appBar: AppBar(title: Text('Stream Message'),),
          backgroundColor: Colors.lightGreen,
          body: ListView.separated(itemBuilder: (BuildContext context, int index){
            return Container(
              child: Center(child: Text('${messages[index]}'),),
            );
          },
              separatorBuilder: (BuildContext context, int index) => const Divider(),
              itemCount: messages.length),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.where_to_vote_sharp),
            onPressed: () {
              sendMessages();
            },
          ),
        )
    );
  }

  sendMessages() async {
    messageStream.getMessages().listen((messageEvent) {
      setState(() {
        print(messageEvent);
        messages.add(messageEvent);
      });
    });
  }
}

/*** COLOR STREAM DEMO
class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  Color bgTextColor = Colors.blueGrey;
  ColorStream colorStream = ColorStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',

      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        backgroundColor: bgTextColor,
        body: Text('Welcome to Stream Color'),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeColor();
          },
        ),
      )
    );
  }

  changeColor() async {
    colorStream.getColors().listen((eventColor) {
      setState(() {
        print(eventColor);
        bgTextColor = eventColor;
      });
    });
  }

}
 ***/