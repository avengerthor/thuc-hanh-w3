import 'package:flutter/material.dart';

class MessageStream{
  Stream<String> getMessages() async* {
    final List<String> messages = [
      "Hello World!",
      "Advance Mobile Development",
      "This is a table",
      "Demo Stream message",
      "Flutter practice laboratory",
      "iPhone 13 pro max 256Gb",
      "Today is Saturday",
      "We are Venom",
      "Funny Coding",
      "My name is Duy",
      "Imagine his surprise when he discovered that the safe was full of pudding",
      "Three generations with six decades of life experience",
      "Sarah ran from the serial killer holding a jug of milk",
      "His get rich quick scheme was to grow a cactus farm",
      "There are no heroes in a punk rock band",
      "Each person who knows you has a different perception of who you are",
      "He spiked his hair green to support his iguana",
      "He put heat on the wound to see what would grow",
      "The tart lemonade quenched her thirst, but not her longing",
      "Nobody has encountered an explosive daisy and lived to tell the tale",
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t){
      int index = t % messages.length;
      return messages[index];
    });
  }
}